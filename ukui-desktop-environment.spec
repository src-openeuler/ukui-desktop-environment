%define debug_package %{nil}
Name:           ukui-desktop-environment
Version:        2.0.2
Release:        22
Summary:        ukui-desktop-environment
License:        GPL-2+
URL:            http://www.ukui.org
Source0:        ukui-desktop-environment-2.0.2.tar.gz
Source1:        50-enable-manual-login.conf
Source2:        desktop
Source3:        ukui-default-screen-setting
Source4:        xrandr.desktop

BuildArch:      noarch

patch1:  	0001-change-default-gtk-theme-and-icon-theme.patch
patch2: 	0001-add-components-list-in-README.patch
patch3:		0001-Add-ukui-themes-as-alternative-and-drop-eom-et.patch
patch4:		0001-update-README.md-file.patch

Requires: ukui = %{version}-%{release}

%description
 The UKUI Desktop Environment is the continuation of MATE. It provides an
 intuitive and attractive desktop environment using traditional metaphors for
 Linux and other Unix-like operating systems.
  .
 UKUI is under active development to add support for new technologies while
 preserving a traditional desktop experience.
 .
 This package installs the standard set of applications that are
 part of the official UKUI release.
 
%package -n ukui
Summary:        ukui-desktop-environment

Requires: ukui-greeter
Requires: ukui-control-center
Requires: qt5-ukui-platformtheme
Requires: ukui-menu
Requires: ukui-panel
Requires: ukui-polkit
Requires: ukui-power-manager
Requires: ukui-screensaver
Requires: ukui-session-manager
Requires: ukui-settings-daemon
Requires: ukui-sidebar
Requires: ukui-themes
Requires: ukui-wallpapers
Requires: ukui-system-monitor
Requires: ukui-window-switch
Requires: peony
Requires: peony-extensions
Requires: kylin-nm
Requires: qt5-ukui-platformtheme
Requires: ukui-media
Requires: ukui-kwin

Requires: qt5-qtquickcontrols
Requires: qt5-qtgraphicaleffects
Requires: ubuntukylin-default-settings
Requires: engrampa
#Requires: shotwell
Requires: atril
Requires: google-noto-sans-cjk-ttc-fonts
Requires: mate-terminal

Recommends: firefox
Recommends: time-shutdown
Recommends: kylin-display-switch
Recommends: ukui-notification-daemon
Recommends: pluma
#Recommends: blueman
Recommends: kylin-screenshot kylin-photo-viewer
Recommends: kylin-weather
Recommends: kylin-calculator
Recommends: kylin-video
Recommends: fcitx fcitx-configtool fcitx-qt5
Recommends: kylin-music 
#Recommends: system-config-printer
Recommends: ukui-notebook
Recommends: ukui-search
Recommends: ukui-clock
Recommends: kylin-user-guide
#Recommends: ukylin-feedback-client
Recommends: dejavu-fonts
Recommends: xorg-x11-fonts-others

BuildArch: noarch

%description -n ukui
The UKUI Desktop Environment is the continuation of MATE. It provides an
 intuitive and attractive desktop environment using traditional metaphors for
 Linux and other Unix-like operating systems.
 .
 UKUI is under active development to add support for new technologies while
 preserving a traditional desktop experience.
 .
 This package depends on a very basic set of programs that are necessary to
 start a UKUI desktop environment session. The set of programs includes the
 UKUI file manager (Peony), the UKUI control center and a limited set of
 other obligatory UKUI desktop components.

%package -n ukui-server
Summary:        ukui-desktop-environment

Requires: ukui-greeter
Requires: ukui-control-center
Requires: qt5-ukui-platformtheme
Requires: ukui-menu
Requires: ukui-panel
Requires: ukui-polkit
Requires: ukui-power-manager
Requires: ukui-screensaver
Requires: ukui-session-manager
Requires: ukui-settings-daemon
Requires: ukui-sidebar
Requires: ukui-themes
Requires: ukui-wallpapers
Requires: ukui-system-monitor
Requires: ukui-window-switch
Requires: peony
Requires: peony-extensions
Requires: kylin-nm
Requires: ukui-media
Requires: ubuntukylin-default-settings
Requires: kylin-photo-viewer
Requires: ukui-kwin
Requires: kylin-user-guide

Requires: atril
Requires: mate-terminal
Requires: qt5-qtquickcontrols
Requires: qt5-qtgraphicaleffects
Requires: google-noto-sans-cjk-ttc-fonts
#Requires: system-config-printer

Recommends: engrampa
Recommends: shotwell
Recommends: firefox
Recommends: time-shutdown
Recommends: kylin-display-switch
Recommends: ukui-notification-daemon
Recommends: pluma
Recommends: kylin-screenshot
Recommends: kylin-calculator
Recommends: fcitx fcitx-configtool fcitx-qt5
Recommends: ukui-search
Recommends: dejavu-fonts
Recommends: xorg-x11-fonts-others

BuildArch: noarch

%description -n ukui-server
The UKUI Desktop Environment is the continuation of MATE. It provides an
 intuitive and attractive desktop environment using traditional metaphors for
 Linux and other Unix-like operating systems.
 .
 UKUI is under active development to add support for new technologies while
 preserving a traditional desktop experience.
 .
 This package depends on a very basic set of programs that are necessary to
 start a UKUI desktop environment session. The set of programs includes the
 UKUI file manager (Peony), the UKUI control center and a limited set of
 other obligatory UKUI desktop components.


%package core
Summary:	UKUI Desktop Environment (essential components, metapackage)

Requires: ukui-greeter
Requires: ukui-control-center
Requires: qt5-ukui-platformtheme
Requires: ukui-menu
Requires: ukui-panel
Requires: ukui-polkit
Requires: ukui-power-manager
Requires: ukui-screensaver
Requires: ukui-session-manager
Requires: ukui-settings-daemon
Requires: ukui-sidebar
Requires: ukui-themes
Requires: ukui-wallpapers
Requires: ukui-system-monitor
Requires: ukui-window-switch
Requires: peony
Requires: peony-extensions
Requires: kylin-nm
Requires: qt5-ukui-platformtheme
Requires: ukui-media
Requires: qt5-qtquickcontrols
Requires: qt5-qtgraphicaleffects
Requires: ubuntukylin-default-settings
Requires: engrampa
Requires: shotwell
Requires: atril
Requires: ukui-kwin
Requires: google-noto-sans-cjk-ttc-fonts


%description core
The UKUI Desktop Environment is the continuation of MATE. It provides an
 intuitive and attractive desktop environment using traditional metaphors for
 Linux and other Unix-like operating systems.
 .
 UKUI is under active development to add support for new technologies while
 preserving a traditional desktop experience.
 .
 This package depends on a very basic set of programs that are necessary to
 start a UKUI desktop environment session. The set of programs includes the
 UKUI file manager (Peony), the UKUI control center and a limited set of
 other obligatory UKUI desktop components.

%package -n ukui-enable-manual-login
Summary: Enable manual login in UKUI desktop environment
Requires: ukui-greeter
%description -n ukui-enable-manual-login
Enable manual login in UKUI desktop environment


%prep
%autosetup -n %{name}-%{version} -p1

%build

%install
mkdir -p %{buildroot}/usr/share/lightdm/lightdm.conf.d/
cp -r %{SOURCE1} %{buildroot}/usr/share/lightdm/lightdm.conf.d/

mkdir -p %{buildroot}/etc/sysconfig
cp -r %{SOURCE2} %{buildroot}/etc/sysconfig

mkdir -p %{buildroot}/usr/bin/
cp -r %{SOURCE3} %{buildroot}/usr/bin/
chmod 755 %{buildroot}/usr/bin/ukui-default-screen-setting

mkdir -p %{buildroot}/etc/xdg/autostart/
cp -r %{SOURCE4} %{buildroot}/etc/xdg/autostart/
chmod 644 %{buildroot}/etc/xdg/autostart/xrandr.desktop

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files 
%doc debian/copyright
%doc debian/changelog

%files core
%doc debian/copyright
%doc debian/changelog

%files -n ukui
%doc debian/copyright debian/changelog
%{_sysconfdir}/sysconfig/desktop

%files -n ukui-server
%doc debian/copyright debian/changelog
%{_sysconfdir}/sysconfig/desktop
%{_bindir}/ukui-default-screen-setting
%{_sysconfdir}/xdg/autostart/xrandr.desktop

%files -n ukui-enable-manual-login
%doc debian/copyright debian/changelog
%{_datadir}/lightdm/lightdm.conf.d/50-enable-manual-login.conf

%changelog
* Fri Sep 20 2024 huayadong <huayadong@kylinos.cn> - 2.0.2-22
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add default ukui screen setting

* Mon Sep 02 2024 douyan <douyan@kylinos.cn> - 2.0.2-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: remove Requires system-config-printer to avoid install gnome packages

* Fri Nov 17 2023 peijiankang <peijiankang@kylinos.cn> - 2.0.2-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add desktop for ukui

* Mon Jun 19 2023 peijiankang <peijiankang@kylinos.cn> - 2.0.2-19
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add desktop

* Fri Jun 16 2023 douyan <douyan@kylinos.cn> - 2.0.2-18
- add package ukui-server

* Sun Jun 11 2023 douyan <douyan@kylinos.cn> - 2.0.2-17
- add Recommends: kylin-weather kylin-user-guide

* Wed Dec 7 2022 douyan <douyan@kylinos.cn> - 2.0.2-16
- add Recommends: ukui-notebook ukui-search ukui-clock

* Mon Aug 08 2022 tanyulong <tanyulong@kylinos.cn> - 2.0.2-15
- update README.md file

* Tue Mar 8 2022 douyan <douyan@kylinos.cn> - 2.0.2-14
- add ukui-enable-manual-login package

* Mon Mar 7 2022 douyan <douyan@kylinos.cn> - 2.0.2-13
- remove ukwm

* Wed Mar 2 2022 douyan <douyan@kylinos.cn> - 2.0.2-12
- use ukui-kwin instead of ukwm

* Thu Oct 28 2021 tanyulong <tanyulong@kylinos.cn> - 2.0.2-11
- Add ukui themes as alternative and drop eom et

* Thu Oct 28 2021 tanyulong <tanyulong@kylinos.cn> - 2.0.2-10
- add components list-in README

* Tue Sep 14 2021 douyan <douyan@kylinos.cn> - 2.0.2-9
- add Recommends: kylin-music system-config-printer

* Thu Sep 2 2021 douyan <douyan@kylinos.cn> - 2.0.2-8
- add Recommends: kylin-video fcitx

* Thu Jul 15 2021 tanyulong <tanyulong@kylinos.cn> - 2.0.2-7
- change default gtk theme and icon theme

* Wed Feb 3 2021 lvhan <lvhan@kylinos.cn> - 2.0.2-6
- add Recommends

* Thu Dec 17 2020 douyan <douyan@kylinos.cn> - 2.0.2-5
- add Recommends gnome-screenshot

* Wed Dec 9 2020 douyan <douyan@kylinos.cn> - 2.0.2-4
- add Recommends pluma

* Wed Dec 9 2020 douyan <douyan@kylinos.cn> - 2.0.2-3
- modify dependency

* Mon Nov 23 2020 douyan <douyan@kylinos.cn> - 2.0.2-2
- add google-noto-sans-cjk-ttc-fonts dependency

* Mon Sep 14 2020 douyan <douyan@kylinos.cn> - 2.0.2-1
- Init package for openEuler

